<?php
/**
 * Created by PhpStorm.
 * User: dilanjan
 * Date: 6/8/19
 * Time: 3:43 PM
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Dilanjan_CustomCatalog',
    __DIR__
);
